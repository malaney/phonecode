Phone.com Coding Assignment
===========================

### Requirements

1. git
1. nodejs v10+
2. npm v6+
3. curl
4. mysql 5.7+

### Installation

1. git clone git clone https://malaney@bitbucket.org/malaney/phonecode.git
2. cd phonecode
3. npm install
4. cp .env.dist .env and edit settings 

### Start Service
1. Run `npm run start` to start service.
(Service will be available at http://localhost:3000)
2. Run `curl http://localhost:3000/initialize` to initialize the database.

### Stop Service
`Ctrl-C` within terminal where started

### Add messages
`curl -H "Content-Type: application/json" -X POST http://localhost:3000/message -d '{"from":"+12015551212", "to":"+19735551212","text":"Hello"}'`

### Retrieve messages
`curl http://localhost:3000/message?participants=%2B12015551212`


