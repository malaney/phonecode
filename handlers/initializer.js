const {q} = require('../mysql');

async function init() {
    let sql = `
CREATE TABLE IF NOT EXISTS message (
  \`id\` INT auto_increment primary key,
  \`sender\` VARCHAR(20) NOT NULL,
  \`recipient\` VARCHAR(20) NOT NULL,
  \`msg\` VARCHAR(255) NULL,
  \`created\` DATETIME NOT NULL,
  KEY \`sender_key\` (\`sender\`),
  KEY \`recip_key\` (\`recipient\`)
)
`;
    let result = await q(sql);

    let success = !!(result);
    let message = (success) ? 'DB initialized!' : 'Initialization failed';

    return {success, message};
}

module.exports = {
    init
};

