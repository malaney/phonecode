const HttpError = require('../error');
const {q, getConnection, transaction, commit, rollback} = require('../mysql');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const PNF = require('google-libphonenumber').PhoneNumberFormat;

const minTextLength = 1;
const maxTextLength = 255;
const maxNumParticipants = 10;
const maxNumRecipients = 9;

async function get(req) {
    const {participants} = req.query;
    if (!participants) {
        throw new HttpError('No new participants provided', 400);
    }

    let participantList = participants
        .split(',')
        .slice(0, maxNumParticipants)
        .map(p => p.trim().toString())
        .filter(p => {
            try {
                return phoneUtil.parse(p, 'US')
            } catch (e) {
                // swallow parse error
            }
        });

    if (participantList.length === 0) {
        throw new HttpError('No valid participants provided', 400);
    }

    try {
        let placeHolders = participantList.slice(0).fill('?').join();

        const result = await q(`SELECT msg, sender FROM message WHERE recipient IN (${placeHolders}) LIMIT 10`,
            participantList);
        let success = !!(result);
        let length = result.length;
        return {success, result, length};
    } catch (e) {
        throw new HttpError(e);
    }
}

async function post(req) {
    const {from, to, text} = req.body;

    // validations
    try {
        var fromNumber = phoneUtil.parse(from, 'US');
    } catch (e) {
        throw new HttpError(e, 400);
    }

    let toList = to
        .split(',')
        .slice(0, maxNumRecipients)
        .map(p => p.trim().toString())
        .filter(p => {
            try {
                return phoneUtil.parse(p, 'US')
            } catch (e) {
                // swallow parse error
            }
        })
        .map(p => {
            return `'${p}'`;
        });

    if (text.length < minTextLength) {
        throw new HttpError(`Invalid text msg, msg must be greater than ${minTextLength} chars`, 400);
    }

    if (text.length > maxTextLength) {
        throw new HttpError(`Invalid text msg, msg must be less than ${maxTextLength} chars`, 400);
    }

    let con = await getConnection();
    await transaction(con);
    try {
        let sql = `INSERT INTO message (sender, recipient, msg, created) VALUES (?, ?, ?, NOW())`;
        toList.map((t) => {
            let toNumber = phoneUtil.parse(t, 'US');
            let formattedFrom = phoneUtil.format(fromNumber, PNF.E164);
            let formattedTo = phoneUtil.format(toNumber, PNF.E164);
            const result = q(sql, [formattedFrom, formattedTo, text]);
        });
        await commit(con);
    } catch (e) {
        await rollback(con);
        throw e
    }

    let success = true;
    let message = 'Post successful';

    return {success, message}
}

module.exports = {
    get,
    post
};
