require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');


const app = express();
app.use(bodyParser.json());
app.disable('x-powered-by');
const port = 3000;

const initializer = require('./handlers/initializer');
const msgHandlers = require('./handlers/messages');

const asyncHandler = fn => async (req, res, next) => {
    try {
        const body = await fn(req, res, next);
        res.json({ body })
    } catch (e) {
        !e.code && console.error(e)
        res.json({ error: e.code ? e.message : 'Internal error' })
    }
}

app.get('/', (req, res) => res.send('Hello World!'));
app.get('/initialize', asyncHandler(initializer.init));
app.get('/message', asyncHandler(msgHandlers.get));
app.post('/message', asyncHandler(msgHandlers.post));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

