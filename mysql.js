const mysql = require('mysql')

const MAX_ACTIVE_CONNECTIONS = process.env.MYSQL_MAX_ACTIVE_CONNECTIONS || 3

let pool
function getPool() {
  if (!pool || pool._closed) {
    const host = process.env.DB_HOST
    const user = process.env.DB_USER
    const password = process.env.DB_PASS
    const database = process.env.DB_NAME
    pool = mysql.createPool({
      connectionLimit: MAX_ACTIVE_CONNECTIONS,
      host,
      user,
      password,
      database,
      multipleStatements: true,
    })
  }
  return pool
}

function getConnection() {
  return new Promise((resolve, reject) => {
    getPool().getConnection((err, con) => {
      if (err) return reject(err)
      resolve(con)
    })
  })
}

async function getInstance() {
  const con = await getConnection()
  return {
    q: (query, params = []) => {
      return q(query, params, con)
    },
    release: () => {
      try {
        con.release()
      } catch (e) {
        console.error(e)
      }
    },
    tq: async (query, params = []) => {
      await transaction(con)
      try {
        const res = await q(query, params, con)
        await commit(con)
        return res
      } catch (e) {
        await rollback(con)
        throw e
      }
    }
  }
}

function q(query, params = [], con) {
  return new Promise(async (resolve, reject) => {
    (con || getPool()).query(query, params, (e, res, fields) => {
      if (e) return reject(e)
      resolve(res, fields)
    })
  })
}

function transaction(con) {
  if (!con) throw 'undefined connection'
  return new Promise((resolve, reject) => {
    con.beginTransaction(err => {
      if (err) return reject(err)
      resolve()
    })
  })
}

function rollback(con) {
  if (!con) throw 'undefined connection'
  return new Promise((resolve, reject) => {
    con.rollback(() => {
      try {
        con.release()
      } catch (e) {
        console.error(e)
      }
      resolve()
    })
  })
}

function commit(con) {
  if (!con) throw 'undefined connection'
  return new Promise((resolve, reject) => {
    con.commit(err => {
      try {
        con.release()
      } catch (e) {
        console.error(e)
      }
      if (err) return reject(err)
      resolve()
    })
  })
}

function close() {
  return new Promise((resolve, reject) => {
    getPool().end(function(err) {
      if (err) return reject(err)
      resolve()
    })
  })
}

module.exports = {
  q,
  close,
  getInstance,
  getConnection,
  transaction,
  commit,
  rollback,
}
